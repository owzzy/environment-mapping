#version 450 core

layout (triangles) in;
layout (line_strip, max_vertices = 2) out;

in GSPacket
{
	float intensity;
	vec2 uv;
	vec3 n;
}inputVertex[];

out FSPacket
{
	float intensity;
	vec2 uv;
}outputVertex;

void main()
{
	vec4 p = vec4(0.0);										// declaring p
	vec3 normal = vec3(0.0);								// 

	for(int i=0; i<3; i++)
	{
		normal += inputVertex[i].n;							//
		p += gl_in[i].gl_Position;							//
	}

	p = vec4(p / 3.0);										//
	outputVertex.intensity = 0.0;							//
	gl_Position = p;										//
	EmitVertex();

	outputVertex.intensity = 1.0;							//
	normal = vec3(normalize(normal) * 0.7);					// 0.7 is the length of the normal
	gl_Position = p + vec4(vec3(normal), 0.0);				//
	EmitVertex();

	EndPrimitive();
}
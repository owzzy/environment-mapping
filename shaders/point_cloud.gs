#version 450 core

layout (triangles) in;
layout (points, max_vertices = 3) out;

in GSPacket
{
	float intensity;
	vec2 uv;
}inputVertex[];

out FSPacket
{
	float intensity;
	vec2 uv;
}outputVertex;

void main()
{
	for(int i = 0; i < 3; i++)
	{
		gl_Position = gl_in[i].gl_Position;
		outputVertex.intensity = inputVertex[i].intensity;
		outputVertex.uv = inputVertex[i].uv;
		EmitVertex();
	}
}